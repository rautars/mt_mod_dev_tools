-------------------------
-- Minetest mod dev tools: init

-- Git: https://gitlab.com/rautars/mt_mod_dev_tools
-- License: MIT
-- Credits: rautars
-- Thanks: kikito for inspect lib (github.com/kikito/inspect.lua)
-------------------------

local modpath = minetest.get_modpath("mt_mod_dev_tools");

dev_tools = {
    inspect = dofile(modpath.."/lib_inspect/inspect.lua")
}
