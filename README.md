# Minetest mods dev tools
Utility mod-lib for debugging lua-based minetest mods.

## Third-Party libraries
This mod includes thirdparty libraries listed bellow:

* inspect-lua: helps to inspect lua table structures.

## Usage samples

### Print table content to console
If Minetest is run from console it's easy to see logs and printed messages.

```
local yourTable = {
    field1 = "A",
    field2 = "B"
}

print(dev_tools.inspect(yourTable))
```
